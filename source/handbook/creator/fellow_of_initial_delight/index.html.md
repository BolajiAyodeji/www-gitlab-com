---
layout: markdown_page
title: "Fellow of initial delight"
---

## Description
 
Fellow of initial delight is a role at GitLab that ensures the product creates a great first impression. 
This role was created by GitLab co-founder Dmitriy Zaporozhets (DZ).
It is a mix of user experience, product, and engineering. 

## How it works

Fellow of initial delight will setup and use GitLab features on regular basis. 
Anything that feels broken or simply creates a bad impression should be addressed by an issue or a merge request. 

Point of interest can be: 
* a broken feature
* a feature not being enabled by default
* confusing or broken UI 
* missing or broken link to documentation

The good example of such finding is GitLab not having [Container registry](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/4690) enabled by default. 

## Participate

There is a [creator pairing](/handbook/creator/pairing) program that allows engineers at GitLab to temporaly participate by working with DZ. 